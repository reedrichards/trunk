variable "aws_region" {
  type    = string
  default = "us-east-2"
}

variable "COMMIT_SHA"{
  type = string
}

variable "HANDLER" {
    type = string
}

variable "API_KEY"{}
variable "FTP_USER"{}
variable "FTP_PASS"{}