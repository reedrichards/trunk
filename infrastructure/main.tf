resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_trunk"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",

      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
# TODO more restrictive resource permissions
# !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${TestFunction}:*'
# https://stackoverflow.com/a/58075163
resource "aws_iam_policy" "lambda_policy" {
  # TODO make unique
  name        = "trunk_policy"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    },
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:Scan"
      ],
      "Resource": "arn:aws:dynamodb:us-east-2:150301572911:table/pocket"
    }

  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "policy_attachment" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}


# begin link lambda
module "lambda" {
  source = "./modules/lambda"

  # TODO
  LAMBDA_ENV_VARS = {
    DEBUG               = false
    SUITCASE_TABLE_NAME = "pocket"

  }

  FUNCTION_NAME = "trunk"
  # TODO make param
  BUCKET_NAME       = "trunk.lambdas.jjk.is"
  AWS_REGION        = "us-east-2"
  ROLE              = aws_iam_role.iam_for_lambda
  POLICY_ATTACHMENT = aws_iam_role_policy_attachment.policy_attachment
  COMMIT_SHA        = var.COMMIT_SHA
  RUNTIME           = "python3.8"
  HANDLER           = "main.handler"

}
