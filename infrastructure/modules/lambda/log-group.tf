# https://www.terraform.io/docs/providers/aws/r/cloudwatch_log_group.html
resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name = "/aws/lambda/${var.FUNCTION_NAME}"

  tags = {
    Environment = "production"
  }
}
