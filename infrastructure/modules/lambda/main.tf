data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "../main.py"
  output_path = "../main.zip"
}

data "archive_file" "packages_zip" {
  type        = "zip"
  source_dir  = "../package"
  output_path = "../package.zip"
}



resource "aws_s3_bucket" "b" {
  bucket = var.BUCKET_NAME
  acl    = "private"
  tags = {
    "BucketType" = "LambdaDist"
  }

}

#
resource "aws_s3_bucket_object" "object" {
  bucket = var.BUCKET_NAME
  key    = "${var.COMMIT_SHA}-${var.FUNCTION_NAME}-dist.zip"
  source = data.archive_file.lambda_zip.output_path

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = data.archive_file.lambda_zip.output_md5

  depends_on = [
    aws_s3_bucket.b,

  ]
}

resource "aws_s3_bucket_object" "layer" {
  bucket = var.BUCKET_NAME
  key    = "${var.COMMIT_SHA}-${var.FUNCTION_NAME}-layer.zip"
  source = data.archive_file.packages_zip.output_path

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = data.archive_file.packages_zip.output_md5

  depends_on = [
    aws_s3_bucket.b,

  ]
}

resource "aws_lambda_layer_version" "lambda_layer" {
  layer_name          = "trunk_lib"
  s3_bucket           = var.BUCKET_NAME
  s3_key              = "${var.COMMIT_SHA}-${var.FUNCTION_NAME}-layer.zip"
  source_code_hash    = data.archive_file.lambda_zip.output_base64sha256
  compatible_runtimes = [var.RUNTIME]
  depends_on = [

    aws_s3_bucket_object.layer
  ]
}

resource "aws_lambda_function" "lambda" {
  function_name = var.FUNCTION_NAME
  s3_bucket     = var.BUCKET_NAME
  s3_key        = "${var.COMMIT_SHA}-${var.FUNCTION_NAME}-dist.zip"
  role          = var.ROLE.arn
  handler       = var.HANDLER

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  depends_on = [
    var.POLICY_ATTACHMENT,
    aws_cloudwatch_log_group.lambda_log_group,
    aws_s3_bucket_object.object
  ]

  runtime = var.RUNTIME

  layers = [
    aws_lambda_layer_version.lambda_layer.arn
  ]

  environment {
    variables = var.LAMBDA_ENV_VARS
  }
}
