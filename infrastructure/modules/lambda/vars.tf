variable "LAMBDA_ENV_VARS" {
  type = map
}

variable "FUNCTION_NAME" {
  type = string
}


variable "AWS_REGION" {
  type = string
}

variable "ROLE" {
}

variable "POLICY_ATTACHMENT" {
}

variable "BUCKET_NAME" {
  type = string
}

variable "COMMIT_SHA" {
  type = string
}

variable "RUNTIME" {
  type    = string
  default = "nodejs12.x"
}

variable "HANDLER" {
  type = string
}
