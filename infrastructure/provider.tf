provider "aws" {
  region  = "${var.aws_region}"
  version = "~> 2.70"
}

provider "archive" {
  version = "1.3.0"
}
