import pytest
from main import handler


@pytest.mark.parametrize("event,context", [[{}, {}]])
def test_handler(event, context):
    assert handler(event, context) == 0
