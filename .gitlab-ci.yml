image: python:3.8

variables:
  TF_ROOT: ${CI_PROJECT_DIR}/infrastructure
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/prod
  TF_VAR_COMMIT_SHA: ${CI_COMMIT_SHA}

cache:
  key: example-production
  paths:
    - ${TF_ROOT}/.terraform

stages:
  - build
  - prepare
  - test
  - plan
  - deploy

lint:
  stage: test
  needs: []
  script:
    - pip install flake8
    - flake8 *.py

formatting:
  stage: test
  needs: []
  script:
    - pip install black
    - black --check --diff *.py

typing:
  stage: test
  needs: []
  script:
    - pip install mypy
    - mypy --ignore-missing-imports *.py

build:
    stage: build
    needs: []
    artifacts:
      name: build
      paths:
        - package
    script:
        - if [[ -s requirements.txt ]]; then pip install -r requirements.txt --target ./package/python; else mkdir -p ./package/python && echo "workaround" > ./package/python/readme.txt; fi


init:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  stage: prepare
  needs: []
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform init

validate:
  needs:
    - init
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  stage: test
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform validate

plan:
  needs:
    - validate
    - build
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  stage: plan
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
      - package.zip
      - main.zip
    reports:
      terraform: ${TF_ROOT}/plan.json

apply:
  needs: 
    - plan
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  stage: deploy
  environment:
    name: production
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform apply
  dependencies:
    - plan
  only:
    - master