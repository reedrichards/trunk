# -*- coding: utf-8 -*-

"""Main module."""
import os

import boto3

# from selenium import webdriver
# from selenium.webdriver.common.desired_capabilities import
# DesiredCapabilities


DEBUG = os.environ.get("DEBUG", True)

# cast debug because its difficult to send terraform vars as bools to lambda
if DEBUG == "False" or DEBUG == "false":
    DEBUG = False

# SELENIUM_HUB_URL = os.environ.get("SELENIUM_HUB_URL",
#  "http://selenium-hub:4443/wd/hub")

AWS_ACCESS_KEY_ID = ""
AWS_SECRET_ACCESS_KEY_ID = ""
REGION = os.environ.get("AWS_REGION", "us-east-2")

TABLE_NAME = os.environ.get("SUITCASE_TABLE_NAME")


if DEBUG:
    dynamodb = boto3.resource(
        "dynamodb",
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY_ID,
        region_name=REGION,
    )
else:
    dynamodb = boto3.resource("dynamodb")


# driver = webdriver.Remote(
#     command_executor=SELENIUM_HUB_URL,
#     desired_capabilities=DesiredCapabilities.CHROME,
# )


def handler(event, context):
    """
    Entrypoint for lambda function

    TODO: description
    """
    table = dynamodb.Table(TABLE_NAME)

    response = table.scan()

    while "LastEvaluatedKey" in response:
        print(response["Items"])
        response = table.scan(ExclusiveStartKey=response["LastEvaluatedKey"])
    print(event)
    print(context)
    return 0


if __name__ == "__main__":
    handler({}, {})
